import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Default Routing 
import { HomeComponent } from './home/home.component';

// Article Routing
import { ArticlesComponent }        from './core/articles/articles.component';
import { ArticlesAddComponent }     from './core/articles/articles-add/articles-add.component';
import { ArticlesListingComponent } from './core/articles/articles-listing/articles-listing.component';
import { ArticlesDetailComponent }  from './core/articles/articles-detail/articles-detail.component';

// Login Routing
import { LoginComponent } from './core/login/login.component';
import { LoginRegisterComponent } from './core/login/login-register/login-register.component';
import { LoginConnectComponent } from './core/login/login-connect/login-connect.component';

const routes: Routes = [
  { path:"", component: HomeComponent },
  //{ path:"contact", component: ContactComponent },
  //{ path:"error", component: ContactComponent },
  { path:"login", component: LoginComponent ,
    children: [
      { path: '', redirectTo: 'login', pathMatch: 'full' },
      { path:"register", component: LoginRegisterComponent },
      { path:"connect",  component: LoginConnectComponent }, 
    ]  
  },
  { 
    path: 'articles', 
    children: [
      {path:"", component: ArticlesListingComponent },
      {path:"add",  component: ArticlesAddComponent }, 
      {path:":id",  component: ArticlesDetailComponent }, 
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
