import { Component } from '@angular/core';
import { ConnectionService } from '../../connection/connection.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-articles-add',
  templateUrl: './articles-add.component.html',
  styleUrls: ['./articles-add.component.css']
})
export class ArticlesAddComponent {
  articles = [] as any;

  constructor(private connection:ConnectionService) {
    this.connection.setURL("http://localhost:8080/projet/webapi/articles/");
    
    this.connection.read().subscribe(temp => {
      console.warn(temp);
      if(temp instanceof Array) {
        this.articles = temp;
      } else {
        this.articles.push(temp);
      }
    })
  }

  onSubmit(data: any) {
    this.connection.post(data)
  }
}
