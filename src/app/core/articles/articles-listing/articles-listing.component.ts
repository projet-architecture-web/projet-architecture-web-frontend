import { Component, Input } from '@angular/core';
import { ConnectionService } from '../../connection/connection.service';

@Component({
  selector: 'app-articles-listing',
  templateUrl: './articles-listing.component.html',
  styleUrls: ['./articles-listing.component.css']
})
export class ArticlesListingComponent {
  articles = [] as any;

  constructor(private connection:ConnectionService) {
    this.connection.setURL("http://localhost:8080/projet/webapi/articles/");
    
    this.connection.read().subscribe(temp => {
      console.warn(temp);
      if(temp instanceof Array) {
        this.articles = temp;
      } else {
        this.articles.push(temp);
      }
    })

    //this.connection.post();
    //this.connection.update();
    //this.connection.delete(22)
  }
}
