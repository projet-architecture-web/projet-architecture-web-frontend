import { Component } from '@angular/core';

@Component({
  selector: 'app-login-connect',
  templateUrl: './login-connect.component.html',
  styleUrls: ['./login-connect.component.css']
})
export class LoginConnectComponent {
  isConnected = false;

  constructor() {
    this.isConnected = false;   
  }

  onSubmit() {
    console.log("Connect Form : ")
    this.isConnected = true;
  }
}
