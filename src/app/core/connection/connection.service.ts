import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {
  url: string;

  constructor(private http:HttpClient) {
    this.url = ""; 
  }

  // AUTRE METHODES
  setURL(url:string) {
    this.url="http://localhost:8080/projet/webapi/articles/"; 
    return this;
  }

  // METHODES CRUD

  // CREATE 
  post(data:any) {
    data["id"] = 0;
    console.log(data);
    this.http.post(this.url, data).subscribe((res) => {
      console.log(res);
    });
  }

  // READ
  read() {
    return this.http.get(this.url);
  }

  readByID(_id:number) {
    return this.http.get(this.url+_id);
  }

  // UPDATE
  update() {
    let body = {
      "id":9,
      "marque":"Coca",
      "price":3.0,
      "category":2,
    }
    console.log(body);
    this.http.put(this.url+body.id, body).subscribe((res) => {
      console.log(res);
    });
  }

  // DELETE
  delete(_id:number) {
    this.http.delete(this.url+_id).subscribe((res) => {
      console.log(res);
    });
  }


}
