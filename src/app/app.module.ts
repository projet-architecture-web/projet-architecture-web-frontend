// Module externe
import { NgModule } from '@angular/core';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

// Modules locaux
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { ArticlesComponent } from './core/articles/articles.component';
import { ArticlesAddComponent } from './core/articles/articles-add/articles-add.component';
import { ArticlesListingComponent } from './core/articles/articles-listing/articles-listing.component';
import { ArticlesDetailComponent } from './core/articles/articles-detail/articles-detail.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './core/login/login.component';
import { LoginRegisterComponent } from './core/login/login-register/login-register.component';
import { LoginStatusComponent } from './core/login/login-status/login-status.component';
import { LoginConnectComponent } from './core/login/login-connect/login-connect.component';
import { LoginDetailComponent } from './core/login/login-detail/login-detail.component';
//import { MatMenuModule } from '@angular/material/menu';

@NgModule({
  declarations: [
    NavbarComponent,
    AppComponent,
    HeaderComponent,
    FooterComponent,
    NavbarComponent,
    ArticlesComponent,
    ArticlesAddComponent,
    ArticlesListingComponent,
    ArticlesDetailComponent,
    HomeComponent,
    LoginComponent,
    LoginRegisterComponent,
    LoginStatusComponent,
    LoginConnectComponent,
    LoginDetailComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      {path: 'articles', component: ArticlesComponent},
      //{path: 'category', component: FooterComponent},
      //{path: 'panier', component: FooterComponent},
    ]),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
